<%@page import="javax.portlet.RenderRequest"%>
<%@ include file="../init.jsp" %>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<!-- Resource URL -->
<portlet:resourceURL id="<%=ReporteCovidPortletKeys.RESOURCE_REPORTE%>"	var="consultarReporte" />

<div>
	<div class="total-count">
		<p id="totalCases"></p>
		<p id="totalDeaths"></p>	
	</div>
	<table  class="table table-hover" id="tableReport">
		<thead>
			<tr>
				<th><liferay-ui:message key='reportecovid.table.name' /></th>
	            <th><liferay-ui:message key='reportecovid.table.range' /></th>
	            <th><liferay-ui:message key='reportecovid.text.table.cases.reported' /></th>
			</tr>	
		</thead>
		 <tbody></tbody>		
	</table>
</div>

<script>
$(function() {	
	

consultarDatos();
$('#tableReport').DataTable();

});
function consultarDatos(){		
$.ajax({
  	  url: '<%=consultarReporte%>',
  	  method: "POST",
  	  cache:false,
  	  dataType: "json"	  	
  	}).done(function(data) {
  		  console.log("data "+data);
  		  if(data!=null && data!=""){	  			  
  			$("#totalCases").html("<liferay-ui:message key='reportecovid.text.total.cases' />"+" <span>"+data.totalCases+"</span>");
	  	    $("#totalDeaths").html("<liferay-ui:message key='reportecovid.text.total.deaths' />"+" <span>"+data.totalDeaths+"</span>");    	    
	  	
	  	    generarTablaEstadistica(data.casesByState);
  		 }
  	    
  	  });
 }
 
function generarTablaEstadistica(json) {

	let table = $('#tableReport').dataTable({			
			data : json,
			"pagingType" : "simple_numbers",
		 	responsive : true,		
		 	searching: true,		
		 	info: false,	
		 	"bDestroy": true,
		 	"bLengthChange": false,
		 	iDisplayLength: 10,
		 	"ordering": false,
		 	language: {		    
		 	    "sSearch": "",
		 	    "searchPlaceholder": "<liferay-ui:message key='reportecovid.text.table.search.box'/>",
		 	    "oPaginate": {	    	
		 	        sNext: "<liferay-ui:message key='reporte.covid.table.next.page'/>",
		 	        sPrevious: "<liferay-ui:message key='reporte.covid.table.back.page'/>"
		 	    },
		 	    "emptyTable":   '<liferay-ui:message key="msg-empty-table"/>',
		 	    "sZeroRecords": '<liferay-ui:message key="msg-empty-table"/>',
		    },	
			columns : [ 
				{
					className: "text-center",
					data : null,
					render : function(obj, type, row) {							
						return obj.name;
					}
				}, {
					className: "text-center",
					data : null,
					render : function(obj, type, row) {
						return obj.range;
					}
				},{
					className: "text-center",
					data : null,
					render : function(obj, type, row) {
						return obj.casesReported;
					}
				}
			],
			responsive : true
	});   	
}
</script>