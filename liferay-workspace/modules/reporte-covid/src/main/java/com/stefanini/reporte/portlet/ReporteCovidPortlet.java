package com.stefanini.reporte.portlet;

import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.stefanini.reporte.constants.ReporteCovidPortletKeys;
import com.stefanini.rest.client.api.RestApi;
import com.stefanini.rest.client.constans.RestApiConstants;

import java.io.IOException;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author alexa
 */
@Component(immediate = true,
property = { 
		"com.liferay.portlet.display-category=REPORTE COVID",
		"com.liferay.portlet.header-portlet-css=/css/style.css", 
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=ReporteCovid",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/html/reporte.jsp",
		"javax.portlet.name=" + ReporteCovidPortletKeys.REPORTECOVID, 
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" },
service = Portlet.class)
public class ReporteCovidPortlet extends MVCPortlet {

	@Reference
	private RestApi restApi;




	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {

		String action = resourceRequest.getResourceID();
		JSONObject jsonResponse = null;

		if (action.equals(ReporteCovidPortletKeys.RESOURCE_REPORTE)) {
			jsonResponse = restApi.consultarReporte();
			
		}

		resourceResponse.setCharacterEncoding(RestApiConstants.ENCODING);
		resourceResponse.setContentType("application/json");
		resourceResponse.resetBuffer();
		resourceResponse.getWriter().print(jsonResponse);
		resourceResponse.flushBuffer();
		super.serveResource(resourceRequest, resourceResponse);
	}
	
	
	
}
