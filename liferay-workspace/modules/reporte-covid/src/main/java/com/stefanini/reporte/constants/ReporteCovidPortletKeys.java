package com.stefanini.reporte.constants;

/**
 * @author alexa
 */
public class ReporteCovidPortletKeys {

	public static final String REPORTECOVID ="Reporte-portlet";
	//public static final String RESOURCE_REPORTE="/resource/consultar";
	public static final String RESOURCE_REPORTE="consultarReporte";

}