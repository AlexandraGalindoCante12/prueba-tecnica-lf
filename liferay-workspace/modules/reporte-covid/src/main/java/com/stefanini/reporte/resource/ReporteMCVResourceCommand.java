package com.stefanini.reporte.resource;

import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.util.ContentTypes;
import com.stefanini.reporte.constants.ReporteCovidPortletKeys;
import com.stefanini.rest.client.api.RestApi;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;


@Component(immediate=true,
		property = {
				
				"javax.portlet.name=" + ReporteCovidPortletKeys.REPORTECOVID, 
				"javax.portlet.resource-bundle=content.Language", 
				"mvc.command.name=" + ReporteCovidPortletKeys.RESOURCE_REPORTE 
			},
		 service = MVCResourceCommand.class)

public class ReporteMCVResourceCommand extends BaseMVCResourceCommand {

	
	@Reference
	private RestApi restApi;


	@Override
	protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws Exception {
		
	
		JSONObject jsonResponse = null;
		jsonResponse = restApi.consultarReporte();	
		
		System.out.println("***********************");
		
		
		resourceResponse.getWriter().print(jsonResponse.toJSONString());
		resourceResponse.setContentType(ContentTypes.APPLICATION_JSON);
		
	}

}
