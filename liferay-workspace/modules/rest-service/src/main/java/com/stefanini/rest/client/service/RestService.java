package com.stefanini.rest.client.service;

import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.stefanini.rest.client.api.RestApi;
import com.stefanini.rest.client.constans.RestApiConstants;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.osgi.service.component.annotations.Component;

/**
 * @author alexa
 *
 */

@Component(immediate=true, property= {}, service=RestApi.class)
public class RestService implements RestApi{

private static Log log=LogFactoryUtil.getLog(RestService.class);

	@Override
	public JSONObject consultarReporte() {
		String strResponse;
		JSONObject responseWsReporte = null;
		try {

			URL url = new URL(RestApiConstants.WS_REPORTE);

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			strResponse = IOUtils.toString(connection.getInputStream(), RestApiConstants.ENCODING);

			responseWsReporte = JSONFactoryUtil.createJSONObject(strResponse);

			connection.disconnect();
		} catch (IOException | JSONException e) {
			log.error(e.getStackTrace());
		}

		return responseWsReporte;

	}
	
	
	
	
	
	
}