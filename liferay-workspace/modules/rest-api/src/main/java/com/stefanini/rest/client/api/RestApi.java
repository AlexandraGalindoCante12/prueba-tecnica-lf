package com.stefanini.rest.client.api;

import com.liferay.portal.kernel.json.JSONObject;

import aQute.bnd.annotation.ProviderType;

/**
 * @author alexa
 */

@ProviderType
public interface RestApi {

	public JSONObject consultarReporte();

}